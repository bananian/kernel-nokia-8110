#!/bin/sh

set -e

echo '*==>> Preparing to build kernel'
mkdir .toolchain
cd .toolchain
TOOLCHAIN="gcc-linaro-4.9.4-2017.01-x86_64_arm-linux-gnueabihf"
wget "https://releases.linaro.org/components/toolchain/binaries/4.9-2017.01/arm-linux-gnueabihf/$TOOLCHAIN.tar.xz"
tar xJvf "$TOOLCHAIN.tar.xz"
test -d "$TOOLCHAIN"
tcprefix="$(realpath "$TOOLCHAIN")/bin/arm-linux-gnueabihf-"
cd ..
scripts/config --set-str CONFIG_CROSS_COMPILE "$tcprefix"
